from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import TfidfVectorizer
import preprocessor
import numpy as np
from sklearn.metrics import classification_report
from sklearn.feature_selection import SelectPercentile, f_classif
from sklearn.linear_model import LogisticRegressionCV

def load_data():
    X = []
    with open("./data/train-tweets.txt") as f:
        for line in f:
            X.append(line.split('\t')[1])
    Y = []
    with open("./data/train-labels.txt") as f:
        for line in f:
            Y.append(line.split('\t')[1].strip())
    X_test = []
    with open("./data/dev-tweets.txt") as f:
        for line in f:
            X_test.append(line.split('\t')[1])
    Y_test = []
    with open("./data/dev-labels.txt") as f:
        for line in f:
            Y_test.append(line.split('\t')[1].strip())
    X_comp = []
    with open("./data/test-tweets.txt") as f:
        for line in f:
            X_comp.append(line.split('\t')[1])
    print('done loading')
    return (X, Y, X_test, Y_test, X_comp)

def get_topn(vectorizer, clf, class_labels, topn):
    """Prints features with the highest coefficient values, per class"""
    feature_names = vectorizer.get_feature_names()
    my_set = set([])
    for i, class_label in enumerate(class_labels):
        nlimit = topn
        if class_label != 'neutral':
            if class_label == 'positive':
                nlimit = 2*topn
            else:
                nlimit = 3*topn
        top_selected = np.argsort(clf.coef_[i])[-nlimit:]
        print("%s: %s" % (class_label, ",".join(feature_names[j] for j in top_selected)))
        my_set = my_set.union(set([feature_names[j] for j in top_selected]))
    return my_set

if __name__ == '__main__':

    X, Y, X_test, Y_test, X_comp = load_data()
    X = np.array(X)
    Y = np.array(Y)
    X_test = np.array(X_test)
    Y_test = np.array(Y_test)
    X_comp = np.array(X_comp)

    tfidf_ngrams = TfidfVectorizer(tokenizer=preprocessor.tokenize, max_df=0.5, sublinear_tf=True, ngram_range=(2, 2))
    features = tfidf_ngrams.fit_transform(X)
    features_test = tfidf_ngrams.transform(X_test)
    mnb = MultinomialNB()
    y_pred = mnb.fit(features, Y).predict(features_test)
    print(classification_report(Y_test, y_pred))
    top_features_2gram = get_topn(tfidf_ngrams, mnb, ['negative', 'neutral', 'positive'], 150)
    print(len(top_features_2gram))

    tfidf_ngrams = TfidfVectorizer(tokenizer=preprocessor.tokenize, max_df=0.5, sublinear_tf=True)
    selector = SelectPercentile(f_classif, percentile=100)
    features = tfidf_ngrams.fit_transform(X)
    features_test = tfidf_ngrams.transform(X_test)
    selector.fit(features, Y)
    features = selector.transform(features).toarray()
    features_test = selector.transform(features_test).toarray()
    mnb = MultinomialNB()
    y_pred = mnb.fit(features, Y).predict(features_test)
    print(classification_report(Y_test, y_pred))
    top_features_1gram = get_topn(tfidf_ngrams, mnb, ['negative', 'neutral', 'positive'], 600)
    print(len(top_features_1gram))

    top_features = top_features_2gram.union(top_features_1gram)
    tfidf_ngrams = TfidfVectorizer(tokenizer=preprocessor.tokenize, vocabulary=list(top_features), sublinear_tf=True, ngram_range=(1, 2))
    features = tfidf_ngrams.fit_transform(X)
    features_test = tfidf_ngrams.transform(X_test)
    features_comp = tfidf_ngrams.transform(X_comp)
    lrc = LogisticRegressionCV(Cs=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10], penalty='l1', solver='liblinear', cv=10)
    y_pred = lrc.fit(features, Y).predict(features_test)
    print(classification_report(Y_test, y_pred, digits=3))

    y_comp = lrc.fit(features, Y).predict(features_comp)

    newfile = 'kaggle.txt'
    with open(newfile, 'w') as outfile:
        for result in y_comp:
            outfile.write(result+'\n')

