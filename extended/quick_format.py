input1 = 'data/test-tweets.txt'
ids = []
with open(input1, 'r') as ifile:
    for line in ifile:
        ids.append(line.split('\t')[0])

input2 = 'kaggle.txt'
prediction = []
with open(input2, 'r') as ifile:
    for line in ifile:
        prediction.append(line.strip())

output = 'kaggle.txt'
with open(output, 'w') as file:
    for idx, val in enumerate(ids):
        file.write(val + ',' + prediction[idx] + '\n')