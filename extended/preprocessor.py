#!/usr/bin/env python

import re
import itertools
import string
import html.entities
import nltk
from nltk.corpus import stopwords
import emoji

emoticon_string = r"""
    (?:
      [<>]?
      [:;=8]                     # eyes
      [\-o\*\']?                 # optional nose
      [\)\]\(\[dDpP/\:\}\{@\|\\] # mouth      
      |
      [\)\]\(\[dDpP/\:\}\{@\|\\] # mouth
      [\-o\*\']?                 # optional nose
      [:;=8]                     # eyes
      [<>]?
    )"""

phone_string = r"""
    (?:
      (?:            # (international)
        \+?[01]
        [\-\s.]*
      )?            
      (?:            # (area code)
        [\(]?
        \d{3}
        [\-\s.\)]*
      )?    
      \d{3}          # exchange
      [\-\s.]*   
      \d{4}          # base
    )"""

html_tag_string = r"""<[^>]+>"""

twitter_user_string = r"""(?:@[\w_]+)"""

twitter_hash_tag_string = r"""(?:\#+[\w_]+[\w\'_\-]*[\w_]+)"""

url_string = r"""(?:www\.[^\s]+)|(?:https?://[^\s]+)|(?:http[^\s]+)"""

word_with_apostrophe_or_dash_string = r"""(?:[a-z][a-z'\-_]+[a-z])"""

word_with_out_apostrophe_string = r"""(?:[\w_]+)"""

numeric_type_string = r"""(?:[+\-]?\d+[,/.:-]?\d?[+\-]?)|(?:\d+[stndrdh]{2})"""

ellipsis_string = r"""(?:\.(?:\s*\.){1,})"""

non_white_space_string = r"""(?:\S)"""

regex_strings = (
    phone_string,
    emoticon_string,
    html_tag_string,
    twitter_user_string,
    twitter_hash_tag_string,
    url_string,
    word_with_apostrophe_or_dash_string,
    word_with_out_apostrophe_string,
    numeric_type_string,
    ellipsis_string,
    non_white_space_string
    )

regex_exclusion_strings = (
    phone_string,
    html_tag_string,
    # twitter_user_string,
    # twitter_hash_tag_string,
    url_string,
    numeric_type_string,
    ellipsis_string
    )

word_re = re.compile(r"""(%s)""" % "|".join(regex_strings), re.VERBOSE | re.I | re.UNICODE)
emoticon_re = re.compile(regex_strings[1], re.VERBOSE | re.I | re.UNICODE)
exclusion_re = re.compile(r"""(%s)""" % "|".join(regex_exclusion_strings), re.VERBOSE | re.I | re.UNICODE)

replacements_for_tokens_with_apostrophe = {

    r"didn't": "not",
    r"don't": "not",
    r"doesn't": "not",

    r"hadn't": "not",
    r"haven't": "not",
    r"hasn't": "not",

    r"wouldn't": "not",
    r"won't": "not",

    r"can't": "not",
    r"couldn't": "not",

    r"ain't": "not",
    r"aren't": "not",
    r"weren't": "not",

    r"isn't": "not",
    r"wasn't": "not",

    r"they're": "they",
    r"they'r": "they",
    r"they'll": "they",

    r"it's": "it",
    r"it'll": "it",
    r"it'd": "it",

    r"he's": "he",
    r"he'll": "he",
    r"he'd": "he",

    r"she's": "she",
    r"she'll": "she",
    r"she'd": "she",

    r"i'm": "i",
    r"i'll": "i",
    r"i'd": "i",
    r"i'v": "i",
    r"i've": "i",

    r"you're": "you",
    r"you'r": "you",
    r"you'll": "you",
    r"you'd": "you",
    r"you've": "you",
    r"you'v": "you",

    r"u're": "you",
    r"u'r": "you",
    r"u'll": "you",
    r"u'd": "you",
    r"u've": "you",
    r"u'v": "you",

    r"we're": "we",
    r"we'r": "we",
    r"we'll": "we",
    r"we'd": "we",
    r"we've": "we",
    r"we'v": "we",

    r"that'd": "that",
    r"that's": "that",

}

replacement_list = {

    r"r": "are",
    "m": "am",

    r"ur": "you",
    r"im": "i",

    r"u": "you",

    r"didnt": "not",
    r"dont": "not",
    r"doesnt": "not",

    r"hadnt": "not",
    r"havent": "not",
    r"hasnt": "not",

    r"wouldnt": "not",
    r"wont": "not",

    r"cant": "not",
    r"cannot": "not",
    r"couldnt": "not",

    r"aint": "not",
    r"arent": "not",
    r"werent": "not",

    r"isnt": "not",
    r"wasnt": "not",

    r"haha": "ha",
    r"hahaha": "ha",

    r"c": "see",

    r"n": "and"

}

POSITIVE_EMOTION = 'pos_emo_'

NEGATIVE_EMOTION = 'neg_emo_'

NEUTRAL_EMOTION = 'neu_emo_'

positive_emoticon_list = [':d', ':D', '8)', ':-)', ':)', ';)', '(-:', '(:']

negative_emoticon_list = [':/', ':-(', ':(', ':S', ':-S', '):', ':\'(']

def transform_emoticon(emoticon):
    if emoticon in positive_emoticon_list:
        return POSITIVE_EMOTION
    if emoticon in negative_emoticon_list:
        return NEGATIVE_EMOTION
    return emoticon

positive_emoji_list = [
    ':red_heart:',
    ':heart:',
    ':hearts:',
    ':heart_eyes:',
    ':kissing_heart:',
    ':smile:',
    ':ok_hand:',
    ':two_hearts:',
    ':clap:',
    ':simple_smile:',
    ':heart_decoration:',
    ':thumbsup:',
    ':pray:',
    ':v:',
    ':wink:',
    ':raised_hands:',
    ':muscle:',
    ':sunglasses:',
    ':grinning:',
    ':tada:',
    ':relaxed:',
    ':blush:',
    ':smiling_face_with_halo:',
    ':purple_heart:',
    ':face_blowing_a_kiss:',
    ':party_popper:',
    ':green_heart:',
    ':christmas_tree:',
    ':heart_suit:',
    ':four_leaf_clover:',
    ':hundred_points:',
    ':basketball:',
    ':smiling_face_with_sunglasses:',
    ':princess:',
    ':turkey:',
    ':yellow_heart:',
    ':folded_hands:',
    ':smiling_face_with_smiling_eyes:',
    ':kiss_mark:',
    ':thumbs_up:',
    ':clapping_hands:',
    ':fire:',
    ':medium-light_skin_tone:',
    ':medium-dark_skin_tone:',
    ':raising_hands:',
    ':ok_hand:',
    ':grinning_face_with_smiling_eyes:',
    ':light_skin_tone:',
    ':blue_heart:',
    ':smiling_face_with_heart-eyes:',
]

negative_emoji_list = [
    ':unamused:',
    ':pensive:',
    ':worried:',
    ':-1:',
    ':weary_face:',
    ':thumbs_down:',
    ':neutral_face:',
    ':unamused_face:',
    ':face_with_medical_mask:',
    ':crying_face:',
    ':face_with_rolling_eyes:',
    ':loudly_crying_face:',
    ':pouting_face:',
    ':medium_skin_tone:',
]

neutral_emoji_list = [
    ':face_with_tears_of_joy:',
    ':thinking_face:',
]

def transform_emoji(emoji_):

    emoji_ = emoji.demojize(emoji_)

    if emoji_ in positive_emoji_list:
        return POSITIVE_EMOTION
    if emoji_ in negative_emoji_list:
        return NEGATIVE_EMOTION
    if emoji_ in neutral_emoji_list:
        return NEUTRAL_EMOTION
    return emoji_

html_entity_digit_re = re.compile(r"&#\d+;")
html_entity_alpha_re = re.compile(r"&\w+;")
amp = "&amp;"

def html2unicode(s):
    """
    Internal metod that seeks to replace all the HTML entities in
    s with their corresponding unicode characters.
    """
    # First the digits:
    ents = set(html_entity_digit_re.findall(s))
    if len(ents) > 0:
        for ent in ents:
            entnum = ent[2:-1]
            try:
                entnum = int(entnum)
                s = s.replace(ent, chr(entnum))
            except:
                pass
    ents = set(html_entity_alpha_re.findall(s))
    for ent in ents:
        entname = ent[1:-1]
        try:
            s = s.replace(ent, chr(html.entities.name2codepoint[entname]))
        except:
            pass
        s = s.replace(amp, " and ")
    return s

ps = nltk.stem.SnowballStemmer('english')

pre_skip_list = ["!", "…", "?", "”", "“", "’", "‘", "—", "¯", "–", "►"]

# should not skip
post_skip_list = [
    "not", "no",
    'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday,', 'sunday',
    'yesterday', 'today', 'tomorrow',
    'may',
    'time', 'person', 'year', 'way', 'day', 'thing', 'man', 'world', 'life', 'hand', 'part', 'child', 'eye', 'woman', 'place', 'work', 'week', 'case', 'point', 'government',
    'be', 'have', 'do', 'say', 'get', 'get', 'make', 'go', 'know', 'take', 'see', 'come', 'think', 'look', 'want', 'give', 'use', 'find', 'tell', 'ask', 'work',
]
post_skip_list = [ps.stem(x) for x in post_skip_list]

def tokenize(s):

    # Try to ensure unicode:
    try:
        s = str(s)
    except UnicodeDecodeError:
        s = str(s).encode('string_escape')
    # Fix HTML character entitites:
    s = html2unicode(s)

    # Tokenize:

    # replace repeated letters
    s = re.sub(r"""(.)\1+""", r"""\1\1""", s)
    # s = s.replace('-', ' ')

    words = word_re.findall(s)
    words = itertools.filterfalse(exclusion_re.search, words)
    words = map((lambda x: transform_emoticon(x) if (emoticon_re.search(x)) else x.lower()), words)
    words = [transform_emoji(x) for x in words]
    words = map((lambda x: replacements_for_tokens_with_apostrophe[x] if '\'' in x and x in replacements_for_tokens_with_apostrophe else x), words)
    words = map((lambda x: replacement_list[x] if x in replacement_list else x), words)
    words = [x for x in words if not (len(x) == 1 and x in string.punctuation and x not in '?!')]
    words = [x for x in words if x not in pre_skip_list]
    words = [ps.stem(word) for word in words]
    words = [word for word in words if word == 'not' or word == 'no' or (word not in stopwords.words('english'))]

    return words

if __name__ == '__main__':
    samples = [
        """It has been a blessing for many with no Medicaid expansion in Florida. Most jobs don't offer insurance and many onl… https://t.co/X2f5jq7vZX""",
        """\"Flashback Friday to last halloween when I was Hulk Hogan. @HulkHogan  Good vibes are with you, brother! http://t.co/DgGdp6H3AL\"""",
        """Doctors hit campaign trail as race to medical council elections heats up https://t.co/iiFdwb9v0W #homeopathy""",
        """Is anybody going to the radio station tomorrow to see Shawn? Me and my friend may go but we would like to make new friends/meet there (:""",
        """I just found out Naruto didn't become the 5th Hokage....""",
        """\"Prince George reservist who died Saturday just wanted to help people, his father tells @CBCNews http://t.co/rIAuzrJgRE\"""",
        """Season in the sun versi Nirvana rancak gak..slow rockkk...""",
        """If I didnt have you I'd never see the sun. #MTVStars Lady Gaga""",
        """This is cute. #ThisisUs @NBCThisisUs https://t.co/NdXqYl4gJK""",
        """Today is the International Day for the Elimination of Violence Against Women #orangetheworld #UnitedNations #unodc… https://t.co/uYqCTTtUFj""",
        """@THExBIGxNUTxOSU @Bzachman12 Hellllllll ya I would if it had Zac Efron in it ❤️""",
        """While Fidel Castro fan was typing this, irony said cheers to Kim Jong Un 😂😂 https: // t.co / c2pqjDXX86""",
        """❤❤❤Beauty without compromise❤❤❤No talc, no bismuth, no harmful chemicals & no animal testing https://t.co/2kU0MzR2sa""",
        """@LeMonde_SydMUN The followers of Kim Jong-un""",
        """@StephandStuff5 @TheSciBabe I'd say 100% of them time when they're considering homeopathy. That's pretty much it. :-)"""
    ]
    for tweet in samples:
        print("========================================================================================================================================")
        print(tweet)
        print(' '.join(tokenize(tweet)))
