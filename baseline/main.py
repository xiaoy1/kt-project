# to input arff
# pip install liac-arff

import arff, numpy as np

def read_arff(path):
    dataset = arff.load(open(path))
    data = np.array(dataset['data'])
    # eliminate tweet id and dependent variable
    X = data[0:data.shape[0],1:(data.shape[1]-1)].astype(np.float).astype(np.integer)
    Y = data[0:data.shape[0],(data.shape[1]-1):data.shape[1]]
    Y[Y == 'negative'] = -1
    Y[Y == 'neutral'] = 0
    Y[Y == 'positive'] = 1
    Y = Y.astype(np.integer).T[0]
    return (X, Y)

X_train, Y_train = read_arff('data/train.arff')
X_dev, Y_dev = read_arff('data/dev.arff')

from sklearn.naive_bayes import MultinomialNB
mnb = MultinomialNB()
y_pred = mnb.fit(X_train, Y_train).predict(X_dev)

# http://scikit-learn.org/stable/modules/model_evaluation.html

from sklearn.metrics import classification_report
target_names = ['negative', 'neutral', 'positive']
print(classification_report(Y_dev, y_pred))
